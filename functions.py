def say_hello(names):
    count = 0
    for name in names:
        print("hello " + name)
        count += 1
    return count


names = ['John', 'Yacine', 'Jack']

total = say_hello(names)
#  say_hello(names)

print("said hello {} times".format(total))

'-------------------------------------------------------'

# how to use *args in functions

def sum(*args):
    total = 0
    for number in args:
        total += number
    print(total)


sum(22, 3)


'---------------------------------------------------------'

# how to ceate a class


class car:
    model = "bmw"
    color = "red"


car1 = car()
car.model = "benz"
print(car.color, car.model)

'----------------------------------------------------------'

# creating a class in real world using function __unit__


class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def greeting(self):
        print("Hi dear " + self.name)


p1 = Person("Amin", 30)
print(p1.name, p1.age)


p2 = Person("Yacine", 27)
print(p2.name, p2.age)


p3 = Person("John", 35)
p3.greeting()















