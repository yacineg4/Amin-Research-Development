from django.contrib import admin
import time

from appone.models import Album, Song, Artist

#admin.site.register(Album)
# admin.site.register(Song)
# instead of the admin.site.register(Song), we can use annotations like the following
# instead of admin.site.register(Album) we can create a class and pass the class to the admin page


@admin.register(Song)
class SongAdmin(admin.ModelAdmin):
    save_on_top = True
    exclude = ('lyrics',)
    autocomplete_fields = ['album']

    list_display = ('name',
                    'album',
                    'duration_human_readable',
                    )
    list_filter = ('album__name', 'album__artist_name',)

    search_fields = ['name', 'album__name', 'album__artist_name']

    def duration_human_readable(self, obj):
        return time.strftime('%M:%S', time.gmtime(obj.duration))
    duration_human_readable.short_description = 'Duration'


class AlbumAdmin(admin.ModelAdmin):
#    readonly_fields = ('release_date',)
    search_fields = ['name', 'artist_name']

    list_display = ('name', 'artist_name', 'release_date', )
    list_editable = ('artist_name',)

    list_filter = ('artist_name', )


admin.site.register(Album, AlbumAdmin)


@admin.register(Artist)
class ArtistAdmin(admin.ModelAdmin):

    list_display = (
        'full_name',
        'join_date',
        'published',
    )


#admin.site.register(Artist)