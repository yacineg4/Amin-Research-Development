from django import forms


class TestForm(forms.Form):

    CITIES_CHOICES = (
        (0, 'Paris'),
        (1, 'Montreal'),
        (2, 'Toronto'),
    )

    name = forms.CharField(label='Your name', max_length=50)
    email = forms.EmailField(label='Yoyr Email', max_length=50, required=False)
    yes_no = forms.BooleanField(label='Either Yes or No')
    city = forms.ChoiceField(label='Your ciity', choices=CITIES_CHOICES)