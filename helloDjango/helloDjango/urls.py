
from django.contrib import admin
from django.urls import path
from django.urls import register_converter

from appone import views
from apptwo import views as apptwo_views
from apptwo import converters
from django.urls import path, include

from appone import views
from pybox import views as views_pybox


register_converter(converters.TwoDigitDayConverter, 'dd')


urlpatterns = [
    path('admin/', admin.site.urls),

    path('hello/', views.hello),

    path('apptwo/', include('apptwo.urls')),

    path('peoplebook/', include('peoplebook.urls')),

    path('getformdata/', views.get_form_data),

    path('thanks/', views.thanks, name='thanks'),

    path('registerinpybox', views_pybox.register_in_pybox ),

    path('pyboxthanks/<str:name>/', views_pybox.thanks, name='pyboxthanks'),

]

