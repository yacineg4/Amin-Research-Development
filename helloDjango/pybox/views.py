from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

from pybox.forms import PyBoxForm


def thanks(request, name):

    return HttpResponse('Thanks {} ,your Subscription has been processed'.format(name))


def register_in_pybox(request):
    if request.method == 'POST':
        print("In POST processing")

        form = PyBoxForm(request.POST)

        if form.is_valid():
            print('name:', form.cleaned_data['name'])
            print('email:', form.cleaned_data['email'])
            print('sign Up:', form.cleaned_data['sign'])
            print('The plan they have chosen:', form.cleaned_data['plan'])
            name = form.cleaned_data['name']
            # it redirects to urls by the name of thanks
            return HttpResponseRedirect(reverse('pyboxthanks', args=[name]))

    else:

        form = PyBoxForm()

    return render(request, 'pybox/form.html', {'form': form})
