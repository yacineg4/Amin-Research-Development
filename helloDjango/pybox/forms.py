from django import forms


class PyBoxForm(forms.Form):

    PLAN_CHOICES = (
        (0, 'basic'),
        (1, 'premium'),
        (2, 'delux'),
    )

    name = forms.CharField(label='Your name:', max_length=50)
    email = forms.EmailField(label='Your Email:', max_length=50, required=False)
    sign = forms.BooleanField(label='Would you accept to subscribe in the newsletter ??')
    plan = forms.ChoiceField(label='Choose among these plans: ', choices=PLAN_CHOICES)