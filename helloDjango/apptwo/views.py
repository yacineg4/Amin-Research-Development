from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader


def djangorocks(request):
    return HttpResponse("This is a jazzy response")


"""
def picture_detail(request, category, year, month=0, day=0):
    body = "Category : {} , Yeaaar : {} , Monnnth : {} , dayyy : {}".format(category, year, month, day)
    return HttpResponse(body)

"""


def picture_detail(request, category, year=0, month=0, day=0):
    template = loader.get_template('apptwo/index.html')


    picture = {
        'filename': 'mountain.jpg',
        'categories': ['color', 'landscape', ],
    }

    context = {
        'page_title': 'This is the picture detail, typed in views',
        'description': '<p>This <b>picture</b> was taken in Grad4</p>',
        'category': category,
        'year': year,
        'month': month,
        'day': day,
        'picture': picture,

        'pictures': [
            {
                'name': 'a',
                'filename': 'a.jpg',
            },
            {
                'name': 'b',
                'filename': 'b.jpg',
            },
            {
                'name': 'c',
                'filename': 'c.jpg',
            },

        ],
    }

    return HttpResponse(template.render(context, request))

