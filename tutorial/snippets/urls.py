from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views
from rest_framework.urlpatterns import format_suffix_patterns
from .views import SnippetViewSet, UserViewSet


# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'snippets', views.SnippetViewSet)
router.register(r'users', views.UserViewSet)

urlpatterns = [
    # Create a router and register our viewsets with it.
    path('', include(router.urls)),
    # path('snippets/', views.snippet_list),
    # path('snippets/<int:pk>/', views.snippet_detail),
]

# This should be added to work with the class based view
# urlpatterns = [
#     path('snippets/', views.SnippetList.as_view()),
#     path('snippets/<int:pk>/', views.SnippetDetail.as_view()),
# ]
#
# urlpatterns = format_suffix_patterns(urlpatterns)


# urlpatterns += [
#     path('api-auth/', include('rest_framework.urls')),
# ]
