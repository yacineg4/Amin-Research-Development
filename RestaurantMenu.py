# the restaurant exercise
import pprint

pp = pprint.PrettyPrinter(indent=1)

menu = {
    'Poutin':{
        'ingredients':[
            'potato',
            'sause',
            'cheese',
        ]
    },
        'burger':{
            'ingredients':[
                'veniison meet',
                'ketchup',
                'bread',
            ]
    }
}


run = True

while run:
    action = input("Welcome to our restaurant. what do you want today ?? ")

    if action == "help":
        print("please write menu to see the meals we have"
              "write eat<food> to select a food"
              "write exit to leave the restaurant")
    elif action == 'menu':
        pp.pprint(menu)
    elif action.startswith('eat'):
        _, food = action.split(' ')
        print("You have ordered " + food + ". Wait a moment !!")
        if food not in menu:
            print("sorry we don`t serve such meal")
        else:print("Here is your " + food + ", enjoy !!")
        break
    elif action == 'exit':
        break




