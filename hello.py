print('Hello world ! ')

name = "Yacine"   # This a string variable

print(name)

age = 25  # this is an int variable. python is smart

name = "Felix"  # you can re-assign

print("The name is : " + name + " and the age is : " + str(age)) # Concatinating / Creating a string

print(type(age))       # How to verify the type of a variable

Languages = ['java', 'c#', 'Python']    # create a list

print(Languages[1])

ListTest = (False, "Sth went wrong", 44)

print(ListTest[2])

error, message, _ = ListTest  # assigning new variables to the list

print(message)
print(error)
print(_)

#------------------------------------------------------------------
# creating dictionaries

person = {
    "Name" : "Amin",
    "Age" : 30
}

print(person)
print(person ["Name"])
print(person.keys())
print(person.values())

#---------------------------------------------------------

# Loops  !

a = 8
b = 20

if a < b :
    print("a is less than b")
elif a > b :
    print("a is greater than b")
else: print("a is equal to b")

notifyUser = True if a < 10 else False
print(notifyUser)

#------------------------------------------------------------

# Creating dictionaries and if/else
menu = {
    'Tapas': {
        'ingredients': [
            'tomato',
            'olive',
            'chicken',
        ]
    },
        'Fahitas': {
            'ingredients': [
                'tortila',
                'beef',
                'onion'
            ]
        }
    }


print(menu)

food = 'Fahitas'

if food not in menu :
    print(" sorry we dont serve " + food)
else: print("Here you are the " + food + " you have ordered")


# ---------------------------------------------------------------------------

# for loops

theLanguages = ['java', 'c#', 'python', 'php']

#for language in theLanguages :
#    print(language)



for lang in theLanguages:
    print(lang)
    if lang == 'java':
        continue
    elif lang == 'c#':
        break


#-------------------------------------------------------------------------------------

# While loops

password = ''
cancellled = False


while password != 'python':
    print("secret ppassword ?")
    password = input()
    if cancellled:
        print("don`t give up !!")
    elif password == 'exit':
        break

else: print("you have found the secret password !! ")

# ----------------------------------------------------------------------------------------

